﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListenerProject
{
    //Это глупый парсер, который может работать только с двумя числами.
    //Я хотел наконец понять как работать с Listener'om, поэтому сделал что-то простое.
    class Parser
    {
        public double Value1 { get; set; }
        public double Value2 { get; set; }
        public char Action { get; set; }

        public void Parse(string query)
        {
            var _values = query.Split(' ');
            Value1 = Double.Parse(_values[0]);
            Action = Char.Parse(_values[1]);
            Value2 = Double.Parse(_values[2]);
        }

        public double Count() => Value1.DoAction(Value2, Action);
    }

    public static class Extentions
    {
        public static double DoAction(this double value1, double value2, char action)
        {
            double result = 0;
            switch (action)
            {
                case '+':
                    result = value1 + value2;
                    break;
                case '-':
                    result = value1 - value2;
                    break;
                case '*':
                    result = value1 * value2;
                    break;
                case '/':
                    result = value1 / value2;
                    break;
                default:
                    result = -1;
                    break;
            }
            return result;
        }
    }
}
