﻿using System;
using System.Net;
using System.IO;
using System.Threading.Tasks;

namespace ListenerProject
{
    class Program
    {
        public static void Main()
        {
            Parser parser = new Parser();
            HttpListener listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:8888/");
            listener.Start();
            Console.WriteLine("Waiting for connections...");

            while (true)
            {
                HttpListenerContext context = listener.GetContext();
                HttpListenerRequest request = context.Request;
                HttpListenerResponse response = context.Response;
                
                parser.Parse(request.QueryString[0]);
                Console.WriteLine($"v1 = {parser.Value1}, action = {parser.Action}, v2 = {parser.Value2} ");
                Console.WriteLine($"Sum = {parser.Count()}");
                Console.WriteLine("Connection Processing Completed");

                string responseString = $"<html><head><meta charset='utf8'></head><body>Result = {parser.Count()}</body></html>";
                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
                response.ContentLength64 = buffer.Length;
                Stream output = response.OutputStream;
                output.Write(buffer, 0, buffer.Length);
                output.Close();
            }
        }
    }
}
